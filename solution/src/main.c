#include <stdio.h>

#include "bmp/bmp_utils.h"
#include "func_status/rw_status_checker.h"
#include "img/image.h"
#include "transformations/rotate.h"

int main( int argc, char** argv ){
    if (argc == 3) {
    const char* const input_name = argv[1];
    const char* const output_name = argv[2];
    struct image old = {0};
    struct image new= {0}; 

    if (main_func_img(input_name, READ_B, &old) != PROCEDURE_OK){
        delete_img(old);
        return 0;} 

    new = rotate(&old);
    delete_img(old);

    if (main_func_img(output_name, WRITE_B, &new) != PROCEDURE_OK){ 
        delete_img(new);
        return 0;}

    delete_img(new);
    return 0;
    }
return 0;
}
