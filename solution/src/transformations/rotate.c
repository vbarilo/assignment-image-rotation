#include "rotate.h"
#include "stdlib.h"

struct image rotate(struct image const *src) {
    struct image new = create_img(src->height, src->width);    
    for (size_t i = 0; i < src->width; i++) {
        for (size_t j = 0; j < src->height; j++) {
            pixel_setter(&new, new.width-j, i, pixel_getter(src, i, j));
        } 
    } 
    return new;
}
