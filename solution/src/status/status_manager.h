#ifndef STATUS_MANAGER_H
#define STATUS_MANAGER_H

#include <stdio.h>

#include "../img/image.h"

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_ERROR,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    
};

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_ERROR_HEADER,
    WRITE_ERROR_PIXELS,
    WRITE_ERROR_PADDING
};

#endif
