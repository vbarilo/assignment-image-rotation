#ifndef IMAGE_H
#define IMAGE_H

#include <malloc.h>
#include <stdint.h>


struct __attribute__((packed)) pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image {
    size_t width;
    size_t height;
    struct pixel* data;
};

struct image create_img(size_t width, size_t height);

void delete_img(struct image img);

struct pixel pixel_getter(const struct image* const img, size_t i, size_t j);

void pixel_setter(struct image* const img, size_t i, size_t j, struct pixel pixel);


#endif
