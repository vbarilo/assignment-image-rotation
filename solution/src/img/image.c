#include "image.h"


struct image create_img(const size_t w, const size_t h){
    struct image res = (struct image){
    .width = w,
    .height = h,
    .data = malloc(sizeof(struct pixel) * w * h)};
    return res;
}

void delete_img(struct image img){free(img.data);}

void pixel_setter(struct image* const img, size_t i, size_t j, struct pixel pixel){
    img->data[ img->width * j + i -1] = pixel;
}

struct pixel pixel_getter(const struct image* const img, size_t i, size_t j) {
    return img->data[ img->width * j + i ];
}

