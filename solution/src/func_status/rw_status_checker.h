#ifndef RW_STATUS_MANAGER_H
#define RW_STATUS_MANAGER_H

#include "../bmp/bmp_utils.h"
#include "../file/file_manager.h"
#include "../status/status_manager.h"

enum func_status {
    PROCEDURE_OK = 0,
    PROCEDURE_ERR
};

enum func_status main_func_img(const char* const file_name, const enum fop_mode mode, struct image* const img);

#endif
