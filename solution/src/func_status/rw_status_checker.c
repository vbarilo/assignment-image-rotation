#include "rw_status_checker.h"

#include <stdio.h>
#include <string.h>

enum func_status main_func_img(const char* const file_name, const enum fop_mode mode, struct image* const img) {
    FILE* file = NULL;
    switch (mode){
        case WRITE_B:
            if (f_open(&file, file_name, "wb")!=OPEN_OK) return PROCEDURE_ERR;
            if (wr_img(file, *img)!=WRITE_OK) return PROCEDURE_ERR;
            break;
        case READ_B:
            if (f_open(&file, file_name, "rb")!=OPEN_OK) return PROCEDURE_ERR;
            if (rd_img(file, img)!=READ_OK) return PROCEDURE_ERR;
            break;
    }
    if (f_close(file)!=CLOSE_OK) return PROCEDURE_ERR; 
    return PROCEDURE_OK;
    }

