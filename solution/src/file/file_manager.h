#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include <errno.h>
#include <stdio.h>
#include <string.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_ERR
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERR
};

enum fop_mode {
    WRITE_B,
    READ_B,
};

enum open_status f_open(FILE** const result_file, const char* const file_name, const char* const mode);
enum close_status f_close(FILE* const file);

#endif
