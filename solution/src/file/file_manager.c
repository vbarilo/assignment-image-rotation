#include "file_manager.h"

enum open_status f_open(FILE** const res, const char* const file_name, const char* const mode) {
    *res = fopen(file_name, mode);
    if (*res) return OPEN_OK;
    return OPEN_ERR;
}

enum close_status f_close(FILE* const file) {
    if (fclose(file)) return CLOSE_ERR;
    return CLOSE_OK;
}
