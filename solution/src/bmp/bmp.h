#ifndef BMP_H
#define BMP_H

#include "../img/image.h"
#include  <stdint.h>
#include <stdlib.h>

uint8_t get_padding(size_t width);

struct __attribute__((packed)) bmp_header;

#endif
