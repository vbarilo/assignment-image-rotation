#include "bmp.h"

#define PADDING_DIVIDER 4

uint8_t get_padding(size_t width) {
    uint8_t pad_r = width*sizeof(struct pixel) % PADDING_DIVIDER;
    if (pad_r != 0)
    {return PADDING_DIVIDER-pad_r;}
    return 0;
}

