#include "bmp_utils.h"

#define F_TYPE 19778
#define RESERVED 0
#define SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define PELSPERMETER 2834
#define CLRUSED 0
#define CLRIMPORTANT 0

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static enum read_status rd_header(FILE* const file, struct bmp_header* const result) {
    const size_t items_num = fread(result, sizeof(struct bmp_header), 1, file);
    if (items_num!=1) return READ_INVALID_HEADER;
    return READ_OK;
}

enum read_status rd_pixels(FILE *in, struct image *img)
{
    if (img == NULL) {
        return READ_INVALID_HEADER;
    } else {
        const size_t w = img->width;
        const uint8_t padding = get_padding(w);
        for (uint32_t i = 0; i < img->height; ++i) {
    if (img == NULL) {
        return READ_INVALID_BITS;
    } else {
        if (fread(&(img->data[i * img->width]), sizeof(struct pixel), img->width, in) != w) return READ_INVALID_SIGNATURE;
        if (fseek(in, padding, SEEK_CUR)!= 0) return READ_INVALID_BITS;
    }
    }
    }
    return READ_OK;

}

enum read_status rd_img(FILE* file, struct image* img) {
    struct bmp_header header = {0};
    if (rd_header(file, &header)!= READ_OK) return rd_header(file, &header);
    *img = create_img(header.biWidth, header.biHeight);
    if (rd_pixels(file, img)!= READ_OK) return rd_pixels(file, img);
    return READ_OK;
}

static struct bmp_header TEMPLATE = {
        .bfType = F_TYPE,
        .bfReserved = RESERVED,
        .biSize = SIZE,
        .biPlanes = PLANES,
        .biBitCount = BIT_COUNT,
        .biCompression = COMPRESSION,
        .biXPelsPerMeter = PELSPERMETER,    
        .biYPelsPerMeter = PELSPERMETER,    
        .biClrUsed = CLRUSED,
        .biClrImportant = CLRIMPORTANT
};

static struct bmp_header create_header(const struct image* src) {
    struct bmp_header header = TEMPLATE;
    header.biWidth = src->width;
    header.biHeight = src->height;    
    header.biSizeImage = header.biHeight * header.biWidth * sizeof(struct pixel);
    header.bfileSize = header.biSizeImage + sizeof(struct bmp_header);
    header.bOffBits = sizeof(struct bmp_header);
    return header;
}

static enum write_status wr_header(FILE* const file, const struct image src) {
    struct bmp_header header = create_header(&src);
    const size_t items_num = fwrite(&header, sizeof(struct bmp_header), 1, file);
    if (items_num == 1) return WRITE_OK;  
    return WRITE_ERROR_HEADER;
}

static enum write_status wr_pixels (FILE* const file, const struct image src) {
    const uint8_t padding = get_padding(src.width);
    const size_t w = src.width;
    const size_t h = src.height;
    struct pixel *pixels = src.data;
    const uint8_t padds[3] = {0, 0, 0};
    for (size_t i = 0; i < h; ++i) {
        if (fwrite(pixels + i * w, sizeof(struct pixel), w, file) == 0) return WRITE_ERROR_PIXELS;
        if ((padding != 0) && (fwrite(padds, 1, padding, file)) == 0) return WRITE_ERROR_PADDING;
    }
    return WRITE_OK;
}

enum write_status wr_img(FILE* const file, const struct image src) {
    const enum write_status header_status = wr_header(file, src);
    if (header_status!=WRITE_OK) return header_status;
    const enum write_status img_status = wr_pixels(file, src);
    return img_status;
}
