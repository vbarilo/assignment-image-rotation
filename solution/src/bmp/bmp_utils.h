#ifndef BMP_UTILS_H
#define BMP_UTILS_H

#include "../img/image.h"
#include "../status/status_manager.h"
#include "bmp.h"

#include <stdio.h>

enum read_status rd_img(FILE* const file, struct image* const img);

enum write_status wr_img(FILE* const file, const struct image src);

#endif
